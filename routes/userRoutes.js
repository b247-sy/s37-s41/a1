const express = require("express");
const router = express.Router();

const auth = require("../auth");
const userController = require("../controllers/userController");

// Check the email
router.post("/checkEmail", (req, res) => {

	userController.checkEmailExists(req.body).then(
		resultFromController => res.send(resultFromController));
});


// register a user
router.post("/register", (req, res) => {

	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for user authentication
router.post("/login", (req, res) => {

	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for getting user details
router.get("/details", (req, res) => {

	userController.getProfile({userId : req.body.id}).then(resultFromController => res.send(resultFromController));
})

// Allows us to export the "router" object that will be accessed in our "index.js" file
module.exports = router;
